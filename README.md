# haxel
haxel is a command-line tool to generate files using haxe's template system. His main purpose is to convert haxe classes to other language classes, but its usage can be generic

## Getting Started

### Prerequisites
Haxe must be installed in order to generate files. ( https://haxe.org/download/ )

Also, two haxe libraries need to be installed, compiletime and haxe-strings:

```bash
 sudo haxelib setup
```

```bash
 sudo haxelib install compiletime
```

## Contributing

TODO

## Authors

* **Julien CAILLABET** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
